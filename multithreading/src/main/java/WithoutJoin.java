import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * Created by Arsen on 06.07.2018.
 */
public class WithoutJoin {
    public static void main(String[] args) throws InterruptedException {

        int[] array;
        final int THREAD = Runtime.getRuntime().availableProcessors();
        int first = 0; // первый элемент разделенного массива
        Random rand = new Random();

        array = IntStream.range(0, 250000000).map(i -> rand.nextInt(50)).toArray(); // заполнение массива (диапазоном до 250 миллионов) значением от 0 до 50.

        int partArray = array.length / THREAD; // делим массив на части

        AtomicInteger result = new AtomicInteger(0); // результирующая переменная, в которую будет записываться суммированные значения

        CountDownLatch countDownLatch = new CountDownLatch(THREAD);

        long start = System.currentTimeMillis(); // начало отсчета
        for(int i = 0; i < THREAD; ++i) {
            new Thread(new ThreadSum(first, first + partArray, array, result,countDownLatch));
            first += partArray;
        }
        countDownLatch.await();

        String output = "sum: %d \t threads: %d \t TimeMillis: %d \n";
        System.out.printf(output,result.get(),THREAD,(System.currentTimeMillis() - start));

        // линейное суммирование
        int sum = Arrays.stream(array).sum();
        System.out.printf(output,sum,Thread.currentThread().getId(),(System.currentTimeMillis() - start));
    }

    public static class ThreadSum extends Thread {
        private int first;
        private int end;
        private int[] array;
        private AtomicInteger result;
        private CountDownLatch countDownLatch;

        ThreadSum(int first, int end, int[] array, AtomicInteger result, CountDownLatch countDownLatch) {
            this.first = first;
            this.end = end;
            this.array = array;
            this.result = result;
            this.countDownLatch = countDownLatch;
            start();
        }
        @Override
        public void run() {
            int sum = 0;
            System.out.println(currentThread().getName());
            for(int i = first; i < end; ++i) {
                sum += array[i];
            }
            countDownLatch.countDown();
            result.getAndAdd(sum);
        }
    }
}