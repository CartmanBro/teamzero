import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.stream.LongStream;

/**
   Рекурсивное параллельное суммирование используя ForkJoinPool (конкретно в строках 52-57).
 */
public class WithJoin {

    private static long numOfCapacity = 50000000L;
    private static int numOfThreads = Runtime.getRuntime().availableProcessors();
    public static void main(String[] args) {

        Random rand = new Random();
        long[] array = LongStream.range(0, numOfCapacity).map(i -> rand.nextInt(50)).toArray();

        long start = System.currentTimeMillis();

        String form = "sum: %d \t threads: %d \t TimeMillis: %d \n";

        ForkJoinPool forkJoinPool = new ForkJoinPool(numOfThreads);
        System.out.printf(form,forkJoinPool.invoke(new Fork(0,numOfCapacity,array)),numOfThreads,(System.currentTimeMillis() - start));

        long j = 0;
        for (long i = 0; i < array.length; i++) {
            j += array[(int) i];
        }
        System.out.printf(form,j,Thread.currentThread().getId(),(System.currentTimeMillis() - start));
    }

    static class Fork extends RecursiveTask<Long>{
        long from;
        long[] array;
        long to;

        Fork(long from, long to,long[] array) {
            this.from = from;
            this.to = to;
            this.array = array;
        }

        @Override
        protected Long compute() {
            if((from - to) <= numOfCapacity/numOfThreads){
                long j = 0;
                for (long i = from; i < to ; i++) {
                    j += array[(int) i];
                }
                return j;
            }
            else {
                long middle = (from +array.length)/2;
                Fork firstHalf = new Fork(from,middle,array);
                firstHalf.fork();
                Fork secondHalf = new Fork(middle+1,to,array);
                long secondValue = secondHalf.compute();
                return firstHalf.join() + secondValue;
            }
        }
    }
}
