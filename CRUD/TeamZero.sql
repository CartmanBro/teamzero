-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: teamzero
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bank` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank`
--

LOCK TABLES `bank` WRITE;
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` VALUES (4,'Deutsche bank','Frankfurt am Main'),(5,'Сбербанк','Москва');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `count`
--

DROP TABLE IF EXISTS `count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `count` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `money` int(11) DEFAULT NULL,
  `created` timestamp NULL,
  `profile_id` bigint(20) DEFAULT NULL,
  `bank_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile_id` (`profile_id`),
  KEY `fk_count_bank_id` (`bank_id`),
  CONSTRAINT `fk_count_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`),
  CONSTRAINT `fk_count_profile_id` FOREIGN KEY (`profile_id`) REFERENCES `profile` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `count`
--

LOCK TABLES `count` WRITE;
/*!40000 ALTER TABLE `count` DISABLE KEYS */;
INSERT INTO `count` VALUES (6,5000,'2018-07-09 21:47:34',1,5);
/*!40000 ALTER TABLE `count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangelog`
--

DROP TABLE IF EXISTS `databasechangelog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `databasechangelog` (
  `ID` varchar(255) NOT NULL,
  `AUTHOR` varchar(255) NOT NULL,
  `FILENAME` varchar(255) NOT NULL,
  `DATEEXECUTED` datetime NOT NULL,
  `ORDEREXECUTED` int(11) NOT NULL,
  `EXECTYPE` varchar(10) NOT NULL,
  `MD5SUM` varchar(35) DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `TAG` varchar(255) DEFAULT NULL,
  `LIQUIBASE` varchar(20) DEFAULT NULL,
  `CONTEXTS` varchar(255) DEFAULT NULL,
  `LABELS` varchar(255) DEFAULT NULL,
  `DEPLOYMENT_ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangelog`
--

LOCK TABLES `databasechangelog` WRITE;
/*!40000 ALTER TABLE `databasechangelog` DISABLE KEYS */;
INSERT INTO `databasechangelog` VALUES ('00000000000001','jhipster','config/liquibase/changelog/00000000000000_initial_schema.xml','2018-07-09 17:01:50',1,'EXECUTED','7:4e3d76f0193ab0094d61f238ec73b201','createTable tableName=jhi_user; createIndex indexName=idx_user_login, tableName=jhi_user; createIndex indexName=idx_user_email, tableName=jhi_user; createTable tableName=jhi_authority; createTable tableName=jhi_user_authority; addPrimaryKey tableN...','',NULL,'3.5.3',NULL,NULL,'1144909581'),('20180709101207-1','jhipster','config/liquibase/changelog/20180709101207_added_entity_Profile.xml','2018-07-09 17:01:50',2,'EXECUTED','7:a3b1c75bdcb0ddd53bfb8eea2d1ae0c5','createTable tableName=profile','',NULL,'3.5.3',NULL,NULL,'1144909581'),('20180709101208-1','jhipster','config/liquibase/changelog/20180709101208_added_entity_Count.xml','2018-07-09 17:01:50',3,'EXECUTED','7:3ead97586946d619e3599973a067e53e','createTable tableName=count; dropDefaultValue columnName=created, tableName=count','',NULL,'3.5.3',NULL,NULL,'1144909581'),('20180709101209-1','jhipster','config/liquibase/changelog/20180709101209_added_entity_Bank.xml','2018-07-09 17:01:50',4,'EXECUTED','7:a8af3069a73406b259ae2aa58ebb5fc7','createTable tableName=bank','',NULL,'3.5.3',NULL,NULL,'1144909581'),('20180709101210-1','jhipster','config/liquibase/changelog/20180709101210_added_entity_Employees.xml','2018-07-09 17:01:50',5,'EXECUTED','7:8ccb36b461fc0f7528bd06ab579f9866','createTable tableName=employees','',NULL,'3.5.3',NULL,NULL,'1144909581'),('20180709101207-2','jhipster','config/liquibase/changelog/20180709101207_added_entity_constraints_Profile.xml','2018-07-09 17:01:50',6,'EXECUTED','7:d41d8cd98f00b204e9800998ecf8427e','empty','',NULL,'3.5.3',NULL,NULL,'1144909581'),('20180709101208-2','jhipster','config/liquibase/changelog/20180709101208_added_entity_constraints_Count.xml','2018-07-09 17:01:50',7,'EXECUTED','7:58e2f62721a0bdaec337d369807b7623','addForeignKeyConstraint baseTableName=count, constraintName=fk_count_profile_id, referencedTableName=profile; addForeignKeyConstraint baseTableName=count, constraintName=fk_count_bank_id, referencedTableName=bank','',NULL,'3.5.3',NULL,NULL,'1144909581'),('20180709101210-2','jhipster','config/liquibase/changelog/20180709101210_added_entity_constraints_Employees.xml','2018-07-09 17:01:50',8,'EXECUTED','7:39093d588a8e450fe6d77a9918b62c03','addForeignKeyConstraint baseTableName=employees, constraintName=fk_employees_bank_id, referencedTableName=bank','',NULL,'3.5.3',NULL,NULL,'1144909581');
/*!40000 ALTER TABLE `databasechangelog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `databasechangeloglock`
--

DROP TABLE IF EXISTS `databasechangeloglock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `databasechangeloglock` (
  `ID` int(11) NOT NULL,
  `LOCKED` bit(1) NOT NULL,
  `LOCKGRANTED` datetime DEFAULT NULL,
  `LOCKEDBY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `databasechangeloglock`
--

LOCK TABLES `databasechangeloglock` WRITE;
/*!40000 ALTER TABLE `databasechangeloglock` DISABLE KEYS */;
INSERT INTO `databasechangeloglock` VALUES (1,'\0',NULL,NULL);
/*!40000 ALTER TABLE `databasechangeloglock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employees` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `salary` int(11) DEFAULT NULL,
  `bank_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_employees_bank_id` (`bank_id`),
  CONSTRAINT `fk_employees_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
INSERT INTO `employees` VALUES (4,'Иванов Иван Иванович','Москва, ул. Ленина, 32','Менеджер',20000,5),(5,'Денисов Денис Денисович','Москва, ул. Ленина, 32','Уборщик',9000,4);
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_authority`
--

DROP TABLE IF EXISTS `jhi_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jhi_authority` (
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_authority`
--

LOCK TABLES `jhi_authority` WRITE;
/*!40000 ALTER TABLE `jhi_authority` DISABLE KEYS */;
INSERT INTO `jhi_authority` VALUES ('ROLE_ADMIN'),('ROLE_BANKIR'),('ROLE_USER');
/*!40000 ALTER TABLE `jhi_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_audit_event`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jhi_persistent_audit_event` (
  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `principal` varchar(50) NOT NULL,
  `event_date` timestamp NULL DEFAULT NULL,
  `event_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  KEY `idx_persistent_audit_event` (`principal`,`event_date`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_event`
--

LOCK TABLES `jhi_persistent_audit_event` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_event` DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_event` VALUES (1,'admin','2018-07-09 14:02:06','AUTHENTICATION_SUCCESS'),(2,'bank','2018-07-09 14:05:20','AUTHENTICATION_SUCCESS'),(3,'admin','2018-07-09 14:59:59','AUTHENTICATION_SUCCESS'),(4,'admin','2018-07-09 15:00:01','AUTHENTICATION_SUCCESS'),(5,'admin','2018-07-09 15:00:01','AUTHENTICATION_SUCCESS'),(6,'admin','2018-07-09 15:00:02','AUTHENTICATION_SUCCESS'),(7,'admin','2018-07-09 15:00:02','AUTHENTICATION_SUCCESS'),(8,'null','2018-07-09 15:15:49','AUTHENTICATION_FAILURE'),(9,'admin','2018-07-09 15:18:12','AUTHENTICATION_FAILURE'),(10,'admin','2018-07-09 15:18:15','AUTHENTICATION_FAILURE'),(11,'null','2018-07-09 15:18:16','AUTHENTICATION_FAILURE'),(12,'admin','2018-07-09 15:19:05','AUTHENTICATION_SUCCESS'),(13,'null','2018-07-09 15:19:06','AUTHENTICATION_FAILURE'),(14,'admin','2018-07-09 15:19:13','AUTHENTICATION_SUCCESS'),(15,'null','2018-07-09 15:19:14','AUTHENTICATION_FAILURE'),(16,'bank','2018-07-09 17:06:33','AUTHENTICATION_SUCCESS'),(17,'admin','2018-07-09 17:06:47','AUTHENTICATION_SUCCESS'),(18,'bank','2018-07-09 17:07:34','AUTHENTICATION_SUCCESS'),(19,'admin','2018-07-09 17:07:41','AUTHENTICATION_SUCCESS'),(20,'admin','2018-07-09 17:13:41','AUTHENTICATION_SUCCESS'),(21,'admin','2018-07-09 17:21:45','AUTHENTICATION_SUCCESS'),(22,'admin','2018-07-09 17:39:59','AUTHENTICATION_SUCCESS'),(23,'admin','2018-07-09 17:43:30','AUTHENTICATION_SUCCESS'),(24,'admin','2018-07-09 17:47:57','AUTHENTICATION_SUCCESS'),(25,'admin','2018-07-09 18:08:14','AUTHENTICATION_SUCCESS'),(26,'admin','2018-07-09 18:23:26','AUTHENTICATION_SUCCESS'),(27,'admin','2018-07-09 18:32:08','AUTHENTICATION_SUCCESS'),(28,'admin','2018-07-09 18:55:12','AUTHENTICATION_FAILURE'),(29,'admin','2018-07-09 18:55:14','AUTHENTICATION_FAILURE'),(30,'admin','2018-07-09 18:55:22','AUTHENTICATION_SUCCESS'),(31,'admin','2018-07-09 18:55:26','AUTHENTICATION_SUCCESS'),(32,'null','2018-07-09 20:10:31','AUTHENTICATION_FAILURE'),(33,'admin','2018-07-09 20:10:37','AUTHENTICATION_SUCCESS'),(34,'null','2018-07-09 20:10:38','AUTHENTICATION_FAILURE'),(35,'admin','2018-07-09 20:11:30','AUTHENTICATION_SUCCESS'),(36,'admin','2018-07-09 20:21:14','AUTHENTICATION_SUCCESS'),(37,'admin','2018-07-09 20:23:35','AUTHENTICATION_SUCCESS'),(38,'admin','2018-07-09 20:26:02','AUTHENTICATION_SUCCESS'),(39,'admin','2018-07-09 20:27:37','AUTHENTICATION_SUCCESS'),(40,'user','2018-07-09 20:29:02','AUTHENTICATION_SUCCESS'),(41,'user','2018-07-09 20:30:04','AUTHENTICATION_SUCCESS'),(42,'user','2018-07-09 20:31:49','AUTHENTICATION_SUCCESS'),(43,'user','2018-07-09 20:36:08','AUTHENTICATION_SUCCESS'),(44,'admin','2018-07-09 20:38:34','AUTHENTICATION_SUCCESS'),(45,'user','2018-07-09 20:38:55','AUTHENTICATION_SUCCESS'),(46,'user','2018-07-09 20:39:16','AUTHENTICATION_SUCCESS'),(47,'user','2018-07-09 20:42:10','AUTHENTICATION_SUCCESS'),(48,'user','2018-07-09 20:42:17','AUTHENTICATION_SUCCESS'),(49,'user','2018-07-09 20:43:05','AUTHENTICATION_SUCCESS'),(50,'user','2018-07-09 20:43:48','AUTHENTICATION_SUCCESS'),(51,'admin','2018-07-09 20:44:23','AUTHENTICATION_SUCCESS'),(52,'admin','2018-07-09 20:46:00','AUTHENTICATION_SUCCESS'),(53,'bank','2018-07-09 21:08:12','AUTHENTICATION_SUCCESS'),(54,'admin','2018-07-09 21:08:30','AUTHENTICATION_SUCCESS'),(55,'admin','2018-07-09 21:08:41','AUTHENTICATION_SUCCESS'),(56,'bank','2018-07-09 21:19:13','AUTHENTICATION_SUCCESS'),(57,'user','2018-07-09 21:21:02','AUTHENTICATION_SUCCESS'),(58,'admin','2018-07-09 21:21:23','AUTHENTICATION_SUCCESS'),(59,'admin','2018-07-09 21:24:46','AUTHENTICATION_SUCCESS'),(60,'admin','2018-07-09 21:29:34','AUTHENTICATION_SUCCESS'),(61,'admin','2018-07-09 21:31:25','AUTHENTICATION_SUCCESS'),(62,'user','2018-07-09 21:34:13','AUTHENTICATION_SUCCESS'),(63,'admin','2018-07-09 21:43:40','AUTHENTICATION_SUCCESS');
/*!40000 ALTER TABLE `jhi_persistent_audit_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_audit_evt_data`
--

DROP TABLE IF EXISTS `jhi_persistent_audit_evt_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jhi_persistent_audit_evt_data` (
  `event_id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`event_id`,`name`),
  KEY `idx_persistent_audit_evt_data` (`event_id`),
  CONSTRAINT `fk_evt_pers_audit_evt_data` FOREIGN KEY (`event_id`) REFERENCES `jhi_persistent_audit_event` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_audit_evt_data`
--

LOCK TABLES `jhi_persistent_audit_evt_data` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` DISABLE KEYS */;
INSERT INTO `jhi_persistent_audit_evt_data` VALUES (1,'remoteAddress','127.0.0.1'),(2,'remoteAddress','127.0.0.1'),(3,'remoteAddress','127.0.0.1'),(3,'sessionId','ejZTWs2b4FaikKd4FMjn5FhTmu5_Qkt5tojUXE37'),(4,'remoteAddress','127.0.0.1'),(4,'sessionId','6o6PQWeMAjpvrKyFJjjQEHhp-KK84AVNa7D_Kfem'),(5,'remoteAddress','127.0.0.1'),(5,'sessionId','7YJoBawqh26jfHkEbMIGJLei17Q-8LnjiDPvBWWA'),(6,'remoteAddress','127.0.0.1'),(6,'sessionId','xIqpdWW1gNRG-CeLJLE6B1dw6Zi3FXkppZ406oNk'),(7,'remoteAddress','127.0.0.1'),(7,'sessionId','TLOF-cS_G0tRhSmbXjZ7JP899igWsScCvDzAFuke'),(8,'message','Bad credentials'),(8,'remoteAddress','127.0.0.1'),(8,'sessionId','q67z1-V3lrWpby2BcQq5-gNC3Pm4jgZb0jZzA3DG'),(8,'type','org.springframework.security.authentication.BadCredentialsException'),(9,'message','Bad credentials'),(9,'remoteAddress','127.0.0.1'),(9,'type','org.springframework.security.authentication.BadCredentialsException'),(10,'message','Bad credentials'),(10,'remoteAddress','127.0.0.1'),(10,'type','org.springframework.security.authentication.BadCredentialsException'),(11,'message','Bad credentials'),(11,'remoteAddress','127.0.0.1'),(11,'sessionId','Exx0RjCpeImkK6AI2wu5Nlyg9J_nPBG9jHqU9blB'),(11,'type','org.springframework.security.authentication.BadCredentialsException'),(12,'remoteAddress','127.0.0.1'),(13,'message','Bad credentials'),(13,'remoteAddress','127.0.0.1'),(13,'type','org.springframework.security.authentication.BadCredentialsException'),(14,'remoteAddress','127.0.0.1'),(14,'sessionId','CZHzPsLfxZvcbCGR1R3CzbO5AE-QxKRDRX6Uisse'),(15,'message','Bad credentials'),(15,'remoteAddress','127.0.0.1'),(15,'type','org.springframework.security.authentication.BadCredentialsException'),(16,'remoteAddress','127.0.0.1'),(17,'remoteAddress','127.0.0.1'),(17,'sessionId','XtvLQZdmyLibttpzLlzMa99jvWC-KC6Ed-flRTk7'),(18,'remoteAddress','127.0.0.1'),(19,'remoteAddress','127.0.0.1'),(20,'remoteAddress','127.0.0.1'),(20,'sessionId','LPWOa_7ebHugOQsPPB3PfS7amSpgosg426D-jLrE'),(21,'remoteAddress','127.0.0.1'),(21,'sessionId','SkNeatw4kGwuAtSWFj_KWWgZV4zzYdYt-NFOyUuY'),(22,'remoteAddress','127.0.0.1'),(22,'sessionId','Z4i18JKJ-is5PZWFUAQh-Xw51JWb6yJ9gDHGAmpa'),(23,'remoteAddress','127.0.0.1'),(24,'remoteAddress','127.0.0.1'),(25,'remoteAddress','127.0.0.1'),(26,'remoteAddress','127.0.0.1'),(26,'sessionId','9EksOfOglDTItyRUOmw_JwL5JzQuLwfOpQipRkkc'),(27,'remoteAddress','127.0.0.1'),(27,'sessionId','NZBbs28icpUDEKdTkifY56i7Xqr4fpCT9Jcf0tjG'),(28,'message','Bad credentials'),(28,'remoteAddress','127.0.0.1'),(28,'sessionId','gjY3HjLEsY3Yv3GIEY97rfXM7ha356gU_I-5TtVu'),(28,'type','org.springframework.security.authentication.BadCredentialsException'),(29,'message','Bad credentials'),(29,'remoteAddress','127.0.0.1'),(29,'type','org.springframework.security.authentication.BadCredentialsException'),(30,'remoteAddress','127.0.0.1'),(31,'remoteAddress','127.0.0.1'),(31,'sessionId','fZH4eN7sJ-4Hr2YmxFSTSbvu1hXt6d0mrMoPBZQ0'),(32,'message','Bad credentials'),(32,'remoteAddress','127.0.0.1'),(32,'sessionId','_OPUbMeCamm2E1kccLy7GTmIEK6HDw5jVbt3BaLQ'),(32,'type','org.springframework.security.authentication.BadCredentialsException'),(33,'remoteAddress','127.0.0.1'),(34,'message','Bad credentials'),(34,'remoteAddress','127.0.0.1'),(34,'sessionId','zj8JX6Wi3zd1Vzqge-Ncd9DyF5At3W9rBXvh_Afx'),(34,'type','org.springframework.security.authentication.BadCredentialsException'),(35,'remoteAddress','127.0.0.1'),(36,'remoteAddress','127.0.0.1'),(36,'sessionId','BJxZB23ToKMiJI1uAYvq00b7P7OZpNyTMbbMm9oO'),(37,'remoteAddress','127.0.0.1'),(37,'sessionId','gHV_rgk3Q_5reIC9h4itdPrjTz-0uRBSvuKHtdXE'),(38,'remoteAddress','127.0.0.1'),(39,'remoteAddress','0:0:0:0:0:0:0:1'),(40,'remoteAddress','0:0:0:0:0:0:0:1'),(40,'sessionId','3r7HY1Vr7GIG5GBB5b9Nj5nLl1-wu9G5J8kpYcml'),(41,'remoteAddress','0:0:0:0:0:0:0:1'),(41,'sessionId','0YvITFvDWr3nABT-ayJ4AmTIrPKN2kC9d5ZYma5j'),(42,'remoteAddress','0:0:0:0:0:0:0:1'),(42,'sessionId','ZcC0trB8DWQVWt9L5ixemgYoGFxBoN_iD2ldfIwM'),(43,'remoteAddress','0:0:0:0:0:0:0:1'),(43,'sessionId','JFOggdX5hzQ0-CY_JsLpK5fHFhovbALcnHFe65Gx'),(44,'remoteAddress','0:0:0:0:0:0:0:1'),(45,'remoteAddress','0:0:0:0:0:0:0:1'),(45,'sessionId','DcBJPBJdOP4gO--HA7mh82fiRN4cLt4UjeNhhy4w'),(46,'remoteAddress','0:0:0:0:0:0:0:1'),(46,'sessionId','ImzHCtuhNg5itRT3Hl9_-y-1ayqclqoQI8otmQal'),(47,'remoteAddress','0:0:0:0:0:0:0:1'),(48,'remoteAddress','0:0:0:0:0:0:0:1'),(48,'sessionId','SvJvaUHyrhahz0mPj3wYyjXWjZTqZtyoSRbGDt11'),(49,'remoteAddress','0:0:0:0:0:0:0:1'),(50,'remoteAddress','0:0:0:0:0:0:0:1'),(50,'sessionId','FduSBKUDny5ldG_LUE1FJ-toLn3pVN6pBYLG7A3q'),(51,'remoteAddress','0:0:0:0:0:0:0:1'),(52,'remoteAddress','127.0.0.1'),(53,'remoteAddress','127.0.0.1'),(54,'remoteAddress','127.0.0.1'),(55,'remoteAddress','127.0.0.1'),(56,'remoteAddress','127.0.0.1'),(57,'remoteAddress','127.0.0.1'),(58,'remoteAddress','127.0.0.1'),(59,'remoteAddress','127.0.0.1'),(60,'remoteAddress','127.0.0.1'),(61,'remoteAddress','127.0.0.1'),(62,'remoteAddress','127.0.0.1'),(63,'remoteAddress','127.0.0.1');
/*!40000 ALTER TABLE `jhi_persistent_audit_evt_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_persistent_token`
--

DROP TABLE IF EXISTS `jhi_persistent_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jhi_persistent_token` (
  `series` varchar(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `token_value` varchar(20) NOT NULL,
  `token_date` date DEFAULT NULL,
  `ip_address` varchar(39) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`series`),
  KEY `fk_user_persistent_token` (`user_id`),
  CONSTRAINT `fk_user_persistent_token` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_persistent_token`
--

LOCK TABLES `jhi_persistent_token` WRITE;
/*!40000 ALTER TABLE `jhi_persistent_token` DISABLE KEYS */;
INSERT INTO `jhi_persistent_token` VALUES ('33iRPRl8W6i66wXBXaPB',3,'BtCtLpfYJ0mf0CXilD0S','2018-07-09','0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('AHLef9dNNpCOmP4KQDAx',3,'F976R1kIjpV9hR2GKmo3','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('b0XzJYpM9yUDmk5SiQU0',3,'PfJGDetZxzvbKqPo9KHy','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('BI4hgDYLKOWpftjJHrEV',3,'2dYRCOFp2bi254vFbsaP','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('cdmFYafrMGeXb0SOgZcH',3,'CcISS7lkd0koCU0IS3WF','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('gFbIMVk9a7bHg3KFyB9y',3,'uzaGQMkN5kE22fwSwvW3','2018-07-10','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('giq42PqEdeHJ07NDIRrC',3,'t4pvIRguIgXqJj69srH9','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('hIclyXdWWj9s8cVIMd3D',3,'yB2xpQqtIGXQPZ6gUaFK','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('I59DfATKijRHrVTlGaKp',6,'LVSYnQnRZOFsX4LWAnov','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('if0XY75oOQydwiaJp379',4,'V5SITg9hwBUMYewJ7HF9','2018-07-09','0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('j91pe8t8MaTLMR0m3cjh',4,'N54JqxJswvS3eRlWvRw5','2018-07-09','0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('KSs2ZLg9RSFLLM9uinPM',4,'hrMIPcUOVWJpF525Rrt7','2018-07-09','0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('KwhNoIlnsnPwjfupqeNh',3,'LNsZ1IehmGv5o1RltOPX','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('NCHlRnk0OTo5tlht0tYN',3,'tPpsjWEvsHs276MBg49r','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('nrYjgUTf50e0KgUTIxcG',3,'iekHEfIXyNntaocAGp1j','2018-07-09','0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('NWbzjdhO6wZknjBI1i5E',3,'A75v5LHW69jhU7Z6qV04','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('o3knY0z3fEVbDIuBCn6z',3,'aYsfIXRsSsZTtYnaW3wv','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('R2Z49KFrbuI668q4u0Q5',3,'RcOhD9zm2IUazizClB0D','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('Rfs5SNS49jQKufdP1jga',4,'RWtw6ABbXfpVqO9uCzvW','2018-07-09','0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('STaS6jN0V2MXmJ9Shgnq',3,'QC5aJRKXO6VxmaAskeZg','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('u8I6cmPO9naDNL0Cx9Rw',4,'usJhcMMu6vd9ZrH0vSjc','2018-07-09','0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('VRrhPtrXHr0JtTfHq3uS',3,'xKypYQUeVt8MBF6TmgMH','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('w6vNOB64AbhDe9VwmFNY',3,'9kbjwrAcNQsXKyMzVtkl','2018-07-10','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('WFf65uxT7h886zb06coN',3,'k89pMhjofnlSJZChHY89','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('WGkAIgmyTYuE0xtYeGyT',3,'mA9q34dug6Ka2WhLGzxd','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('wWYUCRMK4c9TvDAnPSq9',3,'TEhMbrKkNbTsp9Oid0Xd','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0'),('XPGhPxP4NBTU9Cta1SeY',3,'ak5LFL4w47uSx778f4fz','2018-07-09','0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('YWlV9yg3wWpZSfvVSJbG',4,'XosDkwxeTV8HNgJqb0xZ','2018-07-09','0:0:0:0:0:0:0:1','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'),('Zn8CJXC51g47ZkOKerl5',6,'GdFC8R6gXEwoTAfsOscN','2018-07-09','127.0.0.1','Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0');
/*!40000 ALTER TABLE `jhi_persistent_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user`
--

DROP TABLE IF EXISTS `jhi_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jhi_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password_hash` varchar(60) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image_url` varchar(256) DEFAULT NULL,
  `activated` bit(1) NOT NULL,
  `lang_key` varchar(6) DEFAULT NULL,
  `activation_key` varchar(20) DEFAULT NULL,
  `reset_key` varchar(20) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL,
  `reset_date` timestamp NULL DEFAULT NULL,
  `last_modified_by` varchar(50) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_login` (`login`),
  UNIQUE KEY `idx_user_login` (`login`),
  UNIQUE KEY `ux_user_email` (`email`),
  UNIQUE KEY `idx_user_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user`
--

LOCK TABLES `jhi_user` WRITE;
/*!40000 ALTER TABLE `jhi_user` DISABLE KEYS */;
INSERT INTO `jhi_user` VALUES (1,'system','$2a$10$mE.qmcV0mFU5NcKh73TZx.z4ueI/.bDWbj0T1BYyqP481kGGarKLG','System','System','system@localhost','','','en',NULL,NULL,'system','2018-07-09 14:01:50',NULL,'system',NULL),(2,'anonymoususer','$2a$10$j8S5d7Sr7.8VTOYNviDPOeWX8KcYILUVJBsYV83Y5NtECayypx9lO','Anonymous','User','anonymous@localhost','','','en',NULL,NULL,'system','2018-07-09 14:01:50',NULL,'system',NULL),(3,'admin','$2a$10$gSAhZrxMllrbgj/kkK9UceBPpChGWJA7SYIb1Mqo.n5aNLq1/oRrC','Administrator','Administrator','admin@localhost','','','en',NULL,NULL,'system','2018-07-09 14:01:50',NULL,'system',NULL),(4,'user','$2a$10$VEjxo0jq2YG9Rbk2HmX9S.k1uZBGYUHdUcid3g/vfiEl7lwWgOH/K','User','User','user@localhost','','','en',NULL,NULL,'system','2018-07-09 14:01:50',NULL,'system',NULL),(6,'bank','$2a$10$I9K55iKdAXoUsu1VilZ5..Ll9wVNDSnAPPDPxTIBOCBcqCLlD5j4C','john','Dou','rncb@gmail.com',NULL,'','en',NULL,'93307509847296479582','admin','2018-07-09 14:04:43','2018-07-09 14:04:43','admin','2018-07-09 14:04:43');
/*!40000 ALTER TABLE `jhi_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jhi_user_authority`
--

DROP TABLE IF EXISTS `jhi_user_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `jhi_user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_name` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`,`authority_name`),
  KEY `fk_authority_name` (`authority_name`),
  CONSTRAINT `fk_authority_name` FOREIGN KEY (`authority_name`) REFERENCES `jhi_authority` (`name`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `jhi_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jhi_user_authority`
--

LOCK TABLES `jhi_user_authority` WRITE;
/*!40000 ALTER TABLE `jhi_user_authority` DISABLE KEYS */;
INSERT INTO `jhi_user_authority` VALUES (1,'ROLE_ADMIN'),(3,'ROLE_ADMIN'),(3,'ROLE_BANKIR'),(6,'ROLE_BANKIR'),(1,'ROLE_USER'),(3,'ROLE_USER'),(4,'ROLE_USER');
/*!40000 ALTER TABLE `jhi_user_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `passport` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
INSERT INTO `profile` VALUES (1,'John','Dou','3923942342','NY','j_dou@gmail.com',126121);
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-10  0:49:26
