package com.teamzero.crud.repository;

import com.teamzero.crud.domain.Count;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Count entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CountRepository extends JpaRepository<Count, Long> {

}
