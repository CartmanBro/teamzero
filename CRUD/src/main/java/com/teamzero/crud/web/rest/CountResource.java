package com.teamzero.crud.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.teamzero.crud.service.CountService;
import com.teamzero.crud.web.rest.errors.BadRequestAlertException;
import com.teamzero.crud.web.rest.util.HeaderUtil;
import com.teamzero.crud.service.dto.CountDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Count.
 */
@RestController
@RequestMapping("/api")
public class CountResource {

    private final Logger log = LoggerFactory.getLogger(CountResource.class);

    private static final String ENTITY_NAME = "count";

    private final CountService countService;

    public CountResource(CountService countService) {
        this.countService = countService;
    }

    /**
     * POST  /counts : Create a new count.
     *
     * @param countDTO the countDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new countDTO, or with status 400 (Bad Request) if the count has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/counts")
    @Timed
    public ResponseEntity<CountDTO> createCount(@RequestBody CountDTO countDTO) throws URISyntaxException {
        log.debug("REST request to save Count : {}", countDTO);
        if (countDTO.getId() != null) {
            throw new BadRequestAlertException("A new count cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CountDTO result = countService.save(countDTO);
        return ResponseEntity.created(new URI("/api/counts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /counts : Updates an existing count.
     *
     * @param countDTO the countDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated countDTO,
     * or with status 400 (Bad Request) if the countDTO is not valid,
     * or with status 500 (Internal Server Error) if the countDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/counts")
    @Timed
    public ResponseEntity<CountDTO> updateCount(@RequestBody CountDTO countDTO) throws URISyntaxException {
        log.debug("REST request to update Count : {}", countDTO);
        if (countDTO.getId() == null) {
            return createCount(countDTO);
        }
        CountDTO result = countService.save(countDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, countDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /counts : get all the counts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of counts in body
     */
    @GetMapping("/counts")
    @Timed
    public List<CountDTO> getAllCounts() {
        log.debug("REST request to get all Counts");
        return countService.findAll();
        }

    /**
     * GET  /counts/:id : get the "id" count.
     *
     * @param id the id of the countDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the countDTO, or with status 404 (Not Found)
     */
    @GetMapping("/counts/{id}")
    @Timed
    public ResponseEntity<CountDTO> getCount(@PathVariable Long id) {
        log.debug("REST request to get Count : {}", id);
        CountDTO countDTO = countService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(countDTO));
    }

    /**
     * DELETE  /counts/:id : delete the "id" count.
     *
     * @param id the id of the countDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/counts/{id}")
    @Timed
    public ResponseEntity<Void> deleteCount(@PathVariable Long id) {
        log.debug("REST request to delete Count : {}", id);
        countService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
