/**
 * View Models used by Spring MVC REST controllers.
 */
package com.teamzero.crud.web.rest.vm;
