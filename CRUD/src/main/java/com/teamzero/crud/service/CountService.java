package com.teamzero.crud.service;

import com.teamzero.crud.service.dto.CountDTO;
import java.util.List;

/**
 * Service Interface for managing Count.
 */
public interface CountService {

    /**
     * Save a count.
     *
     * @param countDTO the entity to save
     * @return the persisted entity
     */
    CountDTO save(CountDTO countDTO);

    /**
     * Get all the counts.
     *
     * @return the list of entities
     */
    List<CountDTO> findAll();

    /**
     * Get the "id" count.
     *
     * @param id the id of the entity
     * @return the entity
     */
    CountDTO findOne(Long id);

    /**
     * Delete the "id" count.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
