package com.teamzero.crud.service.mapper;

import com.teamzero.crud.domain.*;
import com.teamzero.crud.service.dto.CountDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Count and its DTO CountDTO.
 */
@Mapper(componentModel = "spring", uses = {ProfileMapper.class, BankMapper.class})
public interface CountMapper extends EntityMapper<CountDTO, Count> {

    @Mapping(source = "profile.id", target = "profileId")
    @Mapping(source = "bank.id", target = "bankId")
    CountDTO toDto(Count count);

    @Mapping(source = "profileId", target = "profile")
    @Mapping(source = "bankId", target = "bank")
    Count toEntity(CountDTO countDTO);

    default Count fromId(Long id) {
        if (id == null) {
            return null;
        }
        Count count = new Count();
        count.setId(id);
        return count;
    }
}
