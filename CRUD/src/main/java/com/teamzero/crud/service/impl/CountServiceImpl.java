package com.teamzero.crud.service.impl;

import com.teamzero.crud.service.CountService;
import com.teamzero.crud.domain.Count;
import com.teamzero.crud.repository.CountRepository;
import com.teamzero.crud.service.dto.CountDTO;
import com.teamzero.crud.service.mapper.CountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Count.
 */
@Service
@Transactional
public class CountServiceImpl implements CountService {

    private final Logger log = LoggerFactory.getLogger(CountServiceImpl.class);

    private final CountRepository countRepository;

    private final CountMapper countMapper;

    public CountServiceImpl(CountRepository countRepository, CountMapper countMapper) {
        this.countRepository = countRepository;
        this.countMapper = countMapper;
    }

    /**
     * Save a count.
     *
     * @param countDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CountDTO save(CountDTO countDTO) {
        log.debug("Request to save Count : {}", countDTO);
        Count count = countMapper.toEntity(countDTO);
        count = countRepository.save(count);
        return countMapper.toDto(count);
    }

    /**
     * Get all the counts.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<CountDTO> findAll() {
        log.debug("Request to get all Counts");
        return countRepository.findAll().stream()
            .map(countMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one count by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public CountDTO findOne(Long id) {
        log.debug("Request to get Count : {}", id);
        Count count = countRepository.findOne(id);
        return countMapper.toDto(count);
    }

    /**
     * Delete the count by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Count : {}", id);
        countRepository.delete(id);
    }
}
