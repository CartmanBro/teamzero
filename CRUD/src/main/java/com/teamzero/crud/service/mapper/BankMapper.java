package com.teamzero.crud.service.mapper;

import com.teamzero.crud.domain.*;
import com.teamzero.crud.service.dto.BankDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Bank and its DTO BankDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BankMapper extends EntityMapper<BankDTO, Bank> {


    @Mapping(target = "counts", ignore = true)
    @Mapping(target = "employees", ignore = true)
    Bank toEntity(BankDTO bankDTO);

    default Bank fromId(Long id) {
        if (id == null) {
            return null;
        }
        Bank bank = new Bank();
        bank.setId(id);
        return bank;
    }
}
