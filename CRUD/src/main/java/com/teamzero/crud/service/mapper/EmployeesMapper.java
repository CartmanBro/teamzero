package com.teamzero.crud.service.mapper;

import com.teamzero.crud.domain.*;
import com.teamzero.crud.service.dto.EmployeesDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Employees and its DTO EmployeesDTO.
 */
@Mapper(componentModel = "spring", uses = {BankMapper.class})
public interface EmployeesMapper extends EntityMapper<EmployeesDTO, Employees> {

    @Mapping(source = "bank.id", target = "bankId")
    EmployeesDTO toDto(Employees employees);

    @Mapping(source = "bankId", target = "bank")
    Employees toEntity(EmployeesDTO employeesDTO);

    default Employees fromId(Long id) {
        if (id == null) {
            return null;
        }
        Employees employees = new Employees();
        employees.setId(id);
        return employees;
    }
}
