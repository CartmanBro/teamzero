package com.teamzero.crud.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

@Entity
@Table(name = "bank")
public class Bank implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "address")
    private String address;

    @OneToMany(mappedBy = "bank")
    @JsonIgnore
    private Set<Count> counts = new HashSet<>();

    @OneToMany(mappedBy = "bank")
    @JsonIgnore
    private Set<Employees> employees = new HashSet<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Bank name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public Bank address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Count> getCounts() {
        return counts;
    }

    public Bank counts(Set<Count> counts) {
        this.counts = counts;
        return this;
    }

    public Bank addCount(Count count) {
        this.counts.add(count);
        count.setBank(this);
        return this;
    }

    public Bank removeCount(Count count) {
        this.counts.remove(count);
        count.setBank(null);
        return this;
    }

    public void setCounts(Set<Count> counts) {
        this.counts = counts;
    }

    public Set<Employees> getEmployees() {
        return employees;
    }

    public Bank employees(Set<Employees> employees) {
        this.employees = employees;
        return this;
    }

    public Bank addEmployees(Employees employees) {
        this.employees.add(employees);
        employees.setBank(this);
        return this;
    }

    public Bank removeEmployees(Employees employees) {
        this.employees.remove(employees);
        employees.setBank(null);
        return this;
    }

    public void setEmployees(Set<Employees> employees) {
        this.employees = employees;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Bank bank = (Bank) o;
        if (bank.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), bank.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Bank{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            "}";
    }
}
