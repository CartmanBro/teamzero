package com.teamzero.crud.domain;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;


@Entity
@Table(name = "count")
public class Count implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "money")
    private Integer money;

    @Column(name = "created")
    private ZonedDateTime created;

    @OneToOne
    @JoinColumn(unique = true)
    private Profile profile;

    @ManyToOne
    @NotNull
    private Bank bank;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMoney() {
        return money;
    }

    public Count money(Integer money) {
        this.money = money;
        return this;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public Count created(ZonedDateTime created) {
        this.created = created;
        return this;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public Profile getProfile() {
        return profile;
    }

    public Count profile(Profile profile) {
        this.profile = profile;
        return this;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public Bank getBank() {
        return bank;
    }

    public Count bank(Bank bank) {
        this.bank = bank;
        return this;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Count count = (Count) o;
        if (count.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), count.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Count{" +
            "id=" + getId() +
            ", money=" + getMoney() +
            ", created='" + getCreated() + "'" +
            "}";
    }
}
