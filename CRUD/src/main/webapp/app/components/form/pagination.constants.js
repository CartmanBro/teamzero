(function() {
    'use strict';

    angular
        .module('teamZeroApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
