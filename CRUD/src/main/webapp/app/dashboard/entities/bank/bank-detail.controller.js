(function() {
    'use strict';

    angular
        .module('teamZeroApp')
        .controller('BankDetailController', BankDetailController);

    BankDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Bank', 'Count', 'Employees'];

    function BankDetailController($scope, $rootScope, $stateParams, previousState, entity, Bank, Count, Employees) {
        var vm = this;

        vm.bank = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('teamZeroApp:bankUpdate', function(event, result) {
            vm.bank = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
