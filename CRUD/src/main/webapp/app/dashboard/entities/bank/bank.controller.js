(function() {
    'use strict';

    angular
        .module('teamZeroApp')
        .controller('BankController', BankController);

    BankController.$inject = ['Bank'];

    function BankController(Bank) {

        var vm = this;

        vm.banks = [];

        loadAll();

        function loadAll() {
            Bank.query(function(result) {
                vm.banks = result;
                vm.searchQuery = null;
            });
        }
    }
})();
