(function() {
    'use strict';

    angular
        .module('teamZeroApp')
        .controller('CountController', CountController);

    CountController.$inject = ['Count'];

    function CountController(Count) {

        var vm = this;

        vm.counts = [];

        loadAll();

        function loadAll() {
            Count.query(function(result) {
                vm.counts = result;
                vm.searchQuery = null;
            });
        }
    }
})();
