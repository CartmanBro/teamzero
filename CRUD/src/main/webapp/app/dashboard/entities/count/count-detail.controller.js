(function() {
    'use strict';

    angular
        .module('teamZeroApp')
        .controller('CountDetailController', CountDetailController);

    CountDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Count', 'Profile', 'Bank'];

    function CountDetailController($scope, $rootScope, $stateParams, previousState, entity, Count, Profile, Bank) {
        var vm = this;

        vm.count = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('teamZeroApp:countUpdate', function(event, result) {
            vm.count = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
