(function() {
    'use strict';
    angular
        .module('teamZeroApp')
        .factory('Count', Count);

    Count.$inject = ['$resource', 'DateUtils'];

    function Count ($resource, DateUtils) {
        var resourceUrl =  'api/counts/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                        data.created = DateUtils.convertDateTimeFromServer(data.created);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
