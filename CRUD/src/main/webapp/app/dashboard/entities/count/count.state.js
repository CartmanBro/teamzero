(function() {
    'use strict';

    angular
        .module('teamZeroApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('count', {
            parent: 'dashboard',
            url: '/count',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Counts'
            },
            views: {
                'content@': {
                    templateUrl: 'app/dashboard/entities/count/counts.html',
                    controller: 'CountController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
            }
        })
        .state('count-detail', {
            parent: 'count',
            url: '/count/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'Count'
            },
            views: {
                'content@': {
                    templateUrl: 'app/dashboard/entities/count/count-detail.html',
                    controller: 'CountDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                entity: ['$stateParams', 'Count', function($stateParams, Count) {
                    return Count.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'count',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('count-detail.edit', {
            parent: 'count-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/dashboard/entities/count/count-dialog.html',
                    controller: 'CountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Count', function(Count) {
                            return Count.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('count.new', {
            parent: 'count',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/dashboard/entities/count/count-dialog.html',
                    controller: 'CountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                money: null,
                                created: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('count', null, { reload: 'count' });
                }, function() {
                    $state.go('count');
                });
            }]
        })
        .state('count.edit', {
            parent: 'count',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/dashboard/entities/count/count-dialog.html',
                    controller: 'CountDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Count', function(Count) {
                            return Count.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('count', null, { reload: 'count' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('count.delete', {
            parent: 'count',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/dashboard/entities/count/count-delete-dialog.html',
                    controller: 'CountDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Count', function(Count) {
                            return Count.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('count', null, { reload: 'count' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
