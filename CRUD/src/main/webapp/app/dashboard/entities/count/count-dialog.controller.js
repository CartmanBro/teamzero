(function() {
    'use strict';

    angular
        .module('teamZeroApp')
        .controller('CountDialogController', CountDialogController);

    CountDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', '$q', 'entity', 'Count', 'Profile', 'Bank'];

    function CountDialogController ($timeout, $scope, $stateParams, $uibModalInstance, $q, entity, Count, Profile, Bank) {
        var vm = this;

        vm.count = entity;
        vm.clear = clear;
        vm.datePickerOpenStatus = {};
        vm.openCalendar = openCalendar;
        vm.save = save;
        vm.profiles = Profile.query({filter: 'count-is-null'});
        $q.all([vm.count.$promise, vm.profiles.$promise]).then(function() {
            if (!vm.count.profileId) {
                return $q.reject();
            }
            return Profile.get({id : vm.count.profileId}).$promise;
        }).then(function(profile) {
            vm.profiles.push(profile);
        });
        vm.banks = Bank.query();

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.count.id !== null) {
                Count.update(vm.count, onSaveSuccess, onSaveError);
            } else {
                Count.save(vm.count, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('teamZeroApp:countUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }

        vm.datePickerOpenStatus.created = false;

        function openCalendar (date) {
            vm.datePickerOpenStatus[date] = true;
        }
    }
})();
