(function() {
    'use strict';

    angular
        .module('teamZeroApp')
        .controller('CountDeleteController',CountDeleteController);

    CountDeleteController.$inject = ['$uibModalInstance', 'entity', 'Count'];

    function CountDeleteController($uibModalInstance, entity, Count) {
        var vm = this;

        vm.count = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Count.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
