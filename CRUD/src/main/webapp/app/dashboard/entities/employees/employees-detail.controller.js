(function() {
    'use strict';

    angular
        .module('teamZeroApp')
        .controller('EmployeesDetailController', EmployeesDetailController);

    EmployeesDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Employees', 'Bank'];

    function EmployeesDetailController($scope, $rootScope, $stateParams, previousState, entity, Employees, Bank) {
        var vm = this;

        vm.employees = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('teamZeroApp:employeesUpdate', function(event, result) {
            vm.employees = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
