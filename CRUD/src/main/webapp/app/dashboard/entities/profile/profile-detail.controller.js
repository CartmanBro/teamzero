(function() {
    'use strict';

    angular
        .module('teamZeroApp')
        .controller('ProfileDetailController', ProfileDetailController);

    ProfileDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Profile', 'Count'];

    function ProfileDetailController($scope, $rootScope, $stateParams, previousState, entity, Profile, Count) {
        var vm = this;

        vm.profile = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('teamZeroApp:profileUpdate', function(event, result) {
            vm.profile = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
