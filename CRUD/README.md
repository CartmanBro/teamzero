## Запуск
1) npm install

2) Система сборки gulp, поэтому - npm install -g gulp-cli

3) Создать базу данных - teamzero. В корне проекта есть дамп базы данных - TeamZero.sql Для доступа к базе данных, необходимо перейти в src\main\resources\config\application-dev.yml и установить собственный username && password в строках 36-37.

4) запуск -  ./mvnw
             gulp

5) пароль/логин для входа:

* admin: admin/admin
    
* user: user/user
    
* bankir: bank/bank


