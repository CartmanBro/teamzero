package com.example.demo.service.mapper;

import com.example.demo.domain.Count;
import com.example.demo.service.dto.CountDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper(componentModel = "spring", uses = {ProfileMapper.class, BankMapper.class})
public interface CountMapper extends EntityMapper<CountDTO, Count> {

    @Mapping(source = "profile.id", target = "profileId")
    @Mapping(source = "bank.id", target = "bankId")
    CountDTO toDto(Count count);

    @Mapping(source = "profileId", target = "profile")
    @Mapping(source = "bankId", target = "bank")
    Count toEntity(CountDTO countDTO);

    default Count fromId(Long id) {
        if (id == null) {
            return null;
        }
        Count count = new Count();
        count.setId(id);
        return count;
    }
}
