package com.example.demo.service.impl;

import com.example.demo.domain.Bank;
import com.example.demo.repository.BankRepository;
import com.example.demo.service.BankService;
import com.example.demo.service.dto.BankDTO;
import com.example.demo.service.mapper.BankMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;


@Service
@Transactional
public class BankServiceImpl implements BankService {

    private final Logger log = LoggerFactory.getLogger(BankServiceImpl.class);

    private final BankRepository bankRepository;

    private final BankMapper bankMapper;

    public BankServiceImpl(BankRepository bankRepository, BankMapper bankMapper) {
        this.bankRepository = bankRepository;
        this.bankMapper = bankMapper;
    }

    @Override
    public BankDTO save(BankDTO bankDTO) {
        log.debug("Request to save Bank : {}", bankDTO);
        Bank bank = bankMapper.toEntity(bankDTO);
        bank = bankRepository.save(bank);
        return bankMapper.toDto(bank);
    }

    @Override
    @Transactional(readOnly = true)
    public List<BankDTO> findAll() {
        LinkedList<BankDTO> bankDTOs = new LinkedList<>();
        for (Bank bank: bankRepository.findAll()){
            BankDTO toDto = bankMapper.toDto(bank);
            bankDTOs.add(toDto);
        }
        return bankDTOs;
    }

    @Override
    @Transactional(readOnly = true)
    public BankDTO findOne(Long id) {
        Bank bank = bankRepository.findById(id).orElse(null);
        return bankMapper.toDto(bank);
    }

    @Override
    public void delete(Long id) {
        bankRepository.deleteById(id);
    }
}
