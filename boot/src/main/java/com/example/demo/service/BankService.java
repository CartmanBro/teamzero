package com.example.demo.service;


import com.example.demo.domain.Bank;
import com.example.demo.service.dto.BankDTO;

import java.util.List;

public interface BankService{

    BankDTO save(BankDTO bankDTO);

    List<BankDTO> findAll();

    BankDTO findOne(Long id);

    void delete(Long id);

}
