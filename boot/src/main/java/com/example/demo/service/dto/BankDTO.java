package com.example.demo.service.dto;


import java.io.Serializable;
import java.util.Objects;

public class BankDTO implements Serializable {

    private Long id;

    private String name;

    private String address;

    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankDTO)) return false;
        BankDTO bankDTO = (BankDTO) o;
        return getId() == bankDTO.getId() &&
                Objects.equals(getName(), bankDTO.getName()) &&
                Objects.equals(getAddress(), bankDTO.getAddress()) &&
                Objects.equals(getDescription(), bankDTO.getDescription());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getName(), getAddress(), getDescription());
    }

    @Override
    public String toString() {
        return "BankDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
