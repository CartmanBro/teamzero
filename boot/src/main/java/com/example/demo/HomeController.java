package com.example.demo;

//import com.example.demo.domain.Customer;
//import com.example.demo.repository.CustomerRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;

import com.example.demo.domain.Bank;
import com.example.demo.domain.Profile;
import com.example.demo.repository.BankRepository;
import com.example.demo.repository.ProfileRepository;
import com.example.demo.service.BankService;
import com.example.demo.service.dto.BankDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.transaction.Transactional;
import java.util.List;

@Controller
public class HomeController {

//    @RequestMapping("/")
//    public String home(){
//
//        return "index";
//    }

    @Autowired
    private ProfileRepository profileRepository;

    @Autowired
    private BankService bankService;
    @Autowired
    private BankRepository bankRepository;
//
//    @GetMapping(path="/add")
//    public @ResponseBody String addNewCustomer(@RequestParam String firstName,
//                                               @RequestParam String lastName,
//                                               @RequestParam String email){
//        Customer customer = new Customer();
//        customer.setFirstName("qjekqbweoq");
//        customer.setLastName("asdjasid");
//        customer.setEmail("asd");
//        customerRepository.save(customer);
//        return "Saved";
//    }
    @GetMapping(path="/all")
    @Transactional
    public @ResponseBody Iterable<Profile> getAllUsers() {
        // This returns a JSON or XML with the users
        System.out.println(profileRepository.findAll());
        return profileRepository.findAll();

    }
    @GetMapping(path="/all-bank")
    @Transactional
    public @ResponseBody
    List<BankDTO> getAllBanks() {
        // This returns a JSON or XML with the users
        System.out.println(bankService.findAll());
        return bankService.findAll();

    }
//    @GetMapping(path="/all-bank")
//    @Transactional
//    public @ResponseBody
//    List<Bank> getAllBanks() {
//        // This returns a JSON or XML with the users
//        System.out.println(bankRepository.findAll());
//        return bankRepository.findAll();
//
//    }
}
