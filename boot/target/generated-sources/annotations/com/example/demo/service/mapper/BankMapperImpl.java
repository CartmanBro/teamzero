package com.example.demo.service.mapper;

import com.example.demo.domain.Bank;
import com.example.demo.service.dto.BankDTO;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-07-25T23:38:43+0300",
    comments = "version: 1.1.0.Final, compiler: javac, environment: Java 1.8.0_171 (Oracle Corporation)"
)
@Component
public class BankMapperImpl implements BankMapper {

    @Override
    public BankDTO toDto(Bank arg0) {
        if ( arg0 == null ) {
            return null;
        }

        BankDTO bankDTO = new BankDTO();

        bankDTO.setId( arg0.getId() );
        bankDTO.setName( arg0.getName() );
        bankDTO.setAddress( arg0.getAddress() );
        bankDTO.setDescription( arg0.getDescription() );

        return bankDTO;
    }

    @Override
    public List<Bank> toEntity(List<BankDTO> arg0) {
        if ( arg0 == null ) {
            return null;
        }

        List<Bank> list = new ArrayList<Bank>();
        for ( BankDTO bankDTO : arg0 ) {
            list.add( toEntity( bankDTO ) );
        }

        return list;
    }

    @Override
    public List<BankDTO> toDto(List<Bank> arg0) {
        if ( arg0 == null ) {
            return null;
        }

        List<BankDTO> list = new ArrayList<BankDTO>();
        for ( Bank bank : arg0 ) {
            list.add( toDto( bank ) );
        }

        return list;
    }

    @Override
    public Bank toEntity(BankDTO bankDTO) {
        if ( bankDTO == null ) {
            return null;
        }

        Bank bank_ = new Bank();

        bank_.setId( bankDTO.getId() );
        bank_.setName( bankDTO.getName() );
        bank_.setAddress( bankDTO.getAddress() );
        bank_.setDescription( bankDTO.getDescription() );

        return bank_;
    }
}
